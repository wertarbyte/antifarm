package de.craften.plugins.antifarm;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wool;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.util.List;

public class AntiFarmPlugin extends JavaPlugin {
    public static AntiFarmPlugin instance;

    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(new SpawnListener(), this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equals("destroyfarms")) {
            World w = null;
            if (sender instanceof Player && args.length == 0)
                w = ((Player) sender).getWorld();
            else if (args.length == 1)
                w = getServer().getWorld(args[0]);

            if (w != null) {
                int removed = destroyFarms(w);
                sender.sendMessage(String.format("AntiFarm blew away %s%d creatures%s in \"%s\".",
                        ChatColor.RED, removed, ChatColor.RESET, w.getName()));

                return true;
            }
        }
        return false;
    }

    /**
     * Destroys all animal farms in the given world.
     *
     * @param world Name of the world to destroy farms in
     * @return Number of removed entities
     */
    public int destroyFarms(World world) {
        Segmenter s = new Segmenter(
                getConfig().getInt("segmentSize.width"),
                getConfig().getInt("segmentSize.length"),
                getConfig().getInt("segmentSize.height"));

        for (Creature a : world.getEntitiesByClass(Creature.class))
            s.add(a);

        int removed = 0;

        //remove animals
        if (getConfig().getStringList("worlds." + world.getName() + ".clean").contains("animals")) {
            for (Entity a : s.getEntities(getAllowed(world, CreatureType.ANIMAL), CreatureType.ANIMAL)) {
                removeEntity(a);
                removed++;
            }
        }
        if (getConfig().getStringList("worlds." + world.getName() + ".clean").contains("villagers")) {
            for (Entity a : s.getEntities(getAllowed(world, CreatureType.VILLAGER), CreatureType.VILLAGER)) {
                removeEntity(a);
                removed++;
            }
        }
        if (getConfig().getStringList("worlds." + world.getName() + ".clean").contains("monsters")) {
            for (Entity a : s.getEntities(getAllowed(world, CreatureType.MONSTER), CreatureType.MONSTER)) {
                removeEntity(a);
                removed++;
            }
        }

        return removed;
    }

    private int getAllowed(World world, CreatureType type) {
        String node;
        switch (type) {
            case MONSTER:
                node = "monstersPerSegment";
                break;
            case VILLAGER:
                node = "villagersPerSegment";
                break;
            case ANIMAL:
                node = "animalsPerSegment";
                break;
            default:
                return Integer.MAX_VALUE;
        }
        return getConfig().getInt("worlds." + world.getName() + "." + node);
    }

    /**
     * Destroys all farms in the segment that contains the given location.
     *
     * @param location Location in the segment that should be cleaned
     * @return Number of removed creatures
     */
    public int destroyFarmsInSegmentAt(Location location, CreatureType type) {
        Segment s = new Segment(
                (int) location.getX() / getConfig().getInt("segmentSize.width"),
                (int) location.getY() / getConfig().getInt("segmentSize.length"),
                (int) location.getZ() / getConfig().getInt("segmentSize.height"));

        for (Creature a : location.getWorld().getEntitiesByClass(Creature.class)) {
            Location locAnimal = a.getLocation();
            Vector pos = new Vector(
                    (int) locAnimal.getX() / getConfig().getInt("segmentSize.width"),
                    (int) locAnimal.getY() / getConfig().getInt("segmentSize.length"),
                    (int) locAnimal.getZ() / getConfig().getInt("segmentSize.height"));
            if (pos.equals(s.getPosition()))
                s.add(a, CreatureType.byCreature(a));
        }

        int removed = 0;
        List<Creature> entities = s.getCreatures(type);
        for (int i = getAllowed(location.getWorld(), type); i < entities.size(); i++) {
            removeEntity(entities.get(i));
            removed++;
        }

        return removed;
    }

    /**
     * Blows up the given entity and drops items according to the configuration.
     *
     * @param entity Entity to remove
     */
    private void removeEntity(Entity entity) {
        World world = entity.getWorld();
        Location l = entity.getLocation();
        world.createExplosion(l.getX(), l.getY(), l.getZ(), 0f, false, false);

        if (getConfig().getBoolean(world.getName() + ".dropItems")) {
            ItemStack drop = null;

            if (entity instanceof Pig)
                drop = new ItemStack(Material.PORK, 1);
            else if (entity instanceof Sheep) {
                drop = (new Wool(((Sheep) entity).getColor())).toItemStack(1);
            } else if (entity instanceof MushroomCow)
                drop = new ItemStack(Material.RED_MUSHROOM, 1);
            else if (entity instanceof Cow)
                drop = new ItemStack(Material.RAW_BEEF, 1);
            else if (entity instanceof Chicken)
                drop = new ItemStack(Material.RAW_CHICKEN, 1);

            if (drop != null)
                world.dropItemNaturally(l, drop);
        }

        entity.remove();
    }
}
