package de.craften.plugins.antifarm;

import org.bukkit.entity.Animals;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Villager;

public enum CreatureType {
    ANIMAL,
    VILLAGER,
    MONSTER,
    OTHER;

    public static CreatureType byCreature(Creature creature) {
        if (creature instanceof Animals)
            return ANIMAL;
        else if (creature instanceof Villager)
            return VILLAGER;
        else if (creature instanceof Monster)
            return MONSTER;
        else
            return OTHER;
    }
}
