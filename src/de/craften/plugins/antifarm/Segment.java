package de.craften.plugins.antifarm;

import org.bukkit.entity.Creature;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Segment {
    private Map<CreatureType, List<Creature>> entities = new HashMap<CreatureType, List<Creature>>();
    private Vector position;

    public Segment(int x, int y, int z) {
        position = new Vector(x, y, z);
    }

    public Segment(Vector pos) {
        position = pos;
    }

    public List<Creature> getCreatures(CreatureType type) {
        if (entities.containsKey(type))
            return entities.get(type);
        else {
            List<Creature> list = new ArrayList<Creature>();
            entities.put(type, list);
            return list;
        }
    }

    public Vector getPosition() {
        return position;
    }

    public void add(Creature creature, CreatureType type) {
        getCreatures(type).add(creature);
    }
}
