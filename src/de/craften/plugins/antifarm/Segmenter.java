package de.craften.plugins.antifarm;

import org.bukkit.Location;
import org.bukkit.entity.Creature;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class Segmenter {
    private ArrayList<Segment> segments = new ArrayList<Segment>();
    private int segWidth;
    private int segLength;
    private int segHeight;

    public Segmenter(int segmentWidth, int segmentLength, int segmentHeight) {
        segWidth = segmentWidth;
        segLength = segmentLength;
        segHeight = segmentHeight;
    }

    public void add(Creature creature) {
        Location l = creature.getLocation();
        getSegmentAt(l.getX(), l.getY(), l.getZ()).add(creature, CreatureType.byCreature(creature));
    }

    public Segment getSegmentAt(double x, double y, double z) {
        Vector target = new Vector((int) x / segWidth, (int) y / segHeight, (int) z / segLength);

        for (Segment s : segments) {
            if (s.getPosition().equals(target))
                return s;
        }

        Segment s = new Segment(target);
        segments.add(s);
        return s;
    }

    /**
     * Gets all creatures of the given type from all segments, except 'offset' entities per segment.
     *
     * @param offset Number of entities not to return per segment
     * @return Entities in the segment that should not be there according to the offset
     */
    public List<Creature> getEntities(int offset, CreatureType type) {
        ArrayList<Creature> entities = new ArrayList<Creature>();
        for (Segment s : segments) {
            for (int i = offset; i < s.getCreatures(type).size(); i++)
                entities.add(s.getCreatures(type).get(i));
        }
        return entities;
    }
}
