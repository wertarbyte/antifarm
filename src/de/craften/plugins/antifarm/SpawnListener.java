package de.craften.plugins.antifarm;

import org.bukkit.entity.Animals;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.util.List;

public class SpawnListener implements Listener {

    @EventHandler
    public void onSpawn(CreatureSpawnEvent e) {
        List<String> clean = AntiFarmPlugin.instance.getConfig()
                .getStringList("worlds." + e.getEntity().getWorld().getName() + ".clean");
        List<String> triggers = AntiFarmPlugin.instance.getConfig()
                .getStringList("worlds." + e.getEntity().getWorld().getName() + ".triggers");

        if (e.getEntity() instanceof Animals && clean.contains("animals")) {
            if (triggers.contains("any")) {
                AntiFarmPlugin.instance.destroyFarmsInSegmentAt(e.getLocation(), CreatureType.ANIMAL);
            } else {
                switch (e.getSpawnReason()) {
                    case BREEDING:
                        if (triggers.contains("breeding"))
                            AntiFarmPlugin.instance.destroyFarmsInSegmentAt(e.getLocation(), CreatureType.ANIMAL);
                        break;
                    case EGG:
                        if (triggers.contains("chickenEggs"))
                            AntiFarmPlugin.instance.destroyFarmsInSegmentAt(e.getLocation(), CreatureType.ANIMAL);
                        break;
                }
            }
        } else if (e.getEntity() instanceof Villager && triggers.contains("any") && clean.contains("villagers")) {
            AntiFarmPlugin.instance.destroyFarmsInSegmentAt(e.getLocation(), CreatureType.VILLAGER);
        } else if (e.getEntity() instanceof Monster && triggers.contains("any") && clean.contains("monsters")) {
            AntiFarmPlugin.instance.destroyFarmsInSegmentAt(e.getLocation(), CreatureType.MONSTER);
        }
    }
}
